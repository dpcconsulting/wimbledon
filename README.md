# Build the application

Use 

```
mvn package
```

from the project directory.

# Install requirements

## Wildfly 10.0

1. Download the latest wildfly from http://wildfly.org/downloads/ (eg: 10.0.0.Final, Java EE7 Full & Web Distribution)

2. Unzip it to a directory

3. Create a user with calling ```bin/add-user.sh```

```
elek@sc2 /home/elek/prog/wildfly-10.0.0.Final
 > ./bin/add-user.sh 

What type of user do you wish to add? 
 a) Management User (mgmt-users.properties) 
 b) Application User (application-users.properties)
(a): a

Enter the details of the new user to add.
Using realm 'ManagementRealm' as discovered from the existing property files.
Username : admin
User 'admin' already exists and is disabled, would you like to... 
 a) Update the existing user password and roles 
 b) Enable the existing user 
 c) Type a new username
(a): a
Password recommendations are listed below. To modify these restrictions edit the add-user.properties configuration file.
 - The password should be different from the username
 - The password should not be one of the following restricted values {root, admin, administrator}
 - The password should contain at least 8 characters, 1 alphabetic character(s), 1 digit(s), 1 non-alphanumeric symbol(s)
Password : 
WFLYDM0102: Password should have at least 1 non-alphanumeric symbol.
Are you sure you want to use the password entered yes/no? yes
Re-enter Password : 
What groups do you want this user to belong to? (Please enter a comma separated list, or leave blank for none)[  ]: 
Updated user 'admin' to file '/home/elek/prog/wildfly-10.0.0.Final/standalone/configuration/mgmt-users.properties'
Updated user 'admin' to file '/home/elek/prog/wildfly-10.0.0.Final/domain/configuration/mgmt-users.properties'
Updated user 'admin' with groups  to file '/home/elek/prog/wildfly-10.0.0.Final/standalone/configuration/mgmt-groups.properties'
Updated user 'admin' with groups  to file '/home/elek/prog/wildfly-10.0.0.Final/domain/configuration/mgmt-groups.properties'
Is this new user going to be used for one AS process to connect to another AS process? 
e.g. for a slave host controller connecting to the master or for a Remoting connection for server to server EJB calls.
yes/no? no 
```

4. Start widfly with ```bin/standalone.sh```

5. Try to login on http://localhost:9990

## Derby

1. Download the latest derby from https://db.apache.org/derby/derby_downloads.html

2. Unpack it to somewhere

3. Start ./bin/startNetworkServer 

```
/home/user/prog/db-derby-10.12.1.1-bin
 > 
Fri Feb 26 11:17:26 CET 2016 : Security manager installed using the Basic server security policy.
Fri Feb 26 11:17:26 CET 2016 : Apache Derby Network Server - 10.12.1.1 - (1704137) started and ready to accept connections on port 1527
```

# Configure and run

### Deploy the derbyclient.jar to Wildfly 

Use the admin screen and deploy the jar from ```/home/user/prog/db-derby-10.12.1.1-bin/lib```

### Register new datasource from the Wildfly admin

Nagivate to Configuration (top) -> Subsystems (left) -> Datasources -> Non-XA -> Add

#### Create Datasource

Choose Datasource: custom

#### Datasource Attributes

```
name: wimbledon
jndi name: java:/jdbc/wimbledon
```

#### JDBC Driver

Select derbyclient.jar from the _Detected Driver_ tab.

#### Create datasource

```
Connection URL: jdbc:derby://localhost:1527/wimbledon;create=true
```

Test the connection!

### Deploy the wimbledon application.

Use Deployments -> Add menu and choose: ```target/Wimbledon-1.0-SNAPSHOT.war``` from the Wimbledon directory.
```

Adter deployment check: http://localhost:8080/Wimbledon