/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wimbledon.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.sql.DataSource;

/**
 *
 * @author vrg
 */
@Singleton
//@Startup
public class DatabaseWiper {

    @Resource(name = "jdbc/wimbledon")
    private DataSource ds;

    @PreDestroy
    public void wipeDatabase() {
        try (Connection conn = ds.getConnection()) {
            wipeDatabase(conn);
        } catch (SQLException ex) {
            throw new EJBException(ex);
        }
    }

    public void wipeDatabase(Connection conn) throws SQLException {
        System.out.println("WIPING DATABASE!!!!");
        List<String> dropStatements = new ArrayList<>();
        addDropConstraintStatements(conn, dropStatements);
        addDropTableStatements(conn, dropStatements);
        addDropSequenceStatements(conn, dropStatements);
        executeDMLQueries(conn, dropStatements);
    }

    private void addDropConstraintStatements(Connection conn, List<String> dropStatements) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                "  SELECT \n"
                + "    'ALTER TABLE ' || T.TABLENAME || ' DROP CONSTRAINT ' || C.CONSTRAINTNAME\n"
                + "FROM \n"
                + "    SYS.SYSCONSTRAINTS C, \n"
                + "    SYS.SYSSCHEMAS S,\n"
                + "    SYS.SYSTABLES T \n"
                + "WHERE \n"
                + "    C.SCHEMAID = S.SCHEMAID \n"
                + "AND \n"
                + "    C.TABLEID = T.TABLEID\n"
                + "AND \n"
                + "    S.SCHEMANAME = CURRENT SCHEMA\n"
                + "AND \n"
                + "    C.\"TYPE\" = 'F'")) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String dropConstraintSQL = rs.getString(1);
                    dropStatements.add(dropConstraintSQL);
                }
            }
        }
    }

    private void addDropTableStatements(Connection conn, List<String> dropStatements) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                "  SELECT \n"
                + "    'DROP TABLE ' || T.TABLENAME\n"
                + "FROM \n"
                + "    SYS.SYSSCHEMAS S,\n"
                + "    SYS.SYSTABLES T \n"
                + "WHERE \n"
                + "    S.SCHEMANAME = CURRENT SCHEMA\n"
                + "AND \n"
                + "    T.SCHEMAID = S.SCHEMAID")) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String dropTableSQL = rs.getString(1);
                    dropStatements.add(dropTableSQL);
                }
            }
        }
    }

    private void addDropSequenceStatements(Connection conn, List<String> dropStatements) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                "SELECT \n"
                + "    'DROP SEQUENCE ' || SS.SEQUENCENAME  || ' RESTRICT'\n"
                + "FROM \n"
                + "    SYS.SYSSCHEMAS S,\n"
                + "    SYS.SYSSEQUENCES SS \n"
                + "WHERE \n"
                + "    SS.SCHEMAID = S.SCHEMAID\n"
                + "AND\n"
                + "    S.SCHEMANAME = CURRENT SCHEMA\n")) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String dropSequenceSQL = rs.getString(1);
                    dropStatements.add(dropSequenceSQL);
                }
            }
        }
    }

    private int executeDMLQueries(Connection conn, List<String> dmlQueries) throws SQLException {
        int affectedRows = 0;
        for (String dmlQuery : dmlQueries) {
            try (PreparedStatement ps = conn.prepareStatement(dmlQuery)) {
                System.out.println("Executing " + dmlQuery);

                affectedRows += ps.executeUpdate();
            }
        }
        return affectedRows;
    }
}
