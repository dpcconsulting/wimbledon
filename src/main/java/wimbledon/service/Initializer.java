package wimbledon.service;

import com.codepoetics.protonpack.StreamUtils;
import com.mysema.query.jpa.impl.JPAQueryFactory;
import wimbledon.entity.Court;
import wimbledon.entity.Gender;
import wimbledon.entity.Round;
import wimbledon.entity.Umpire;
import wimbledon.entity.draw.Draw;
import wimbledon.entity.draw.doubles.MensDoublesDraw;
import wimbledon.entity.draw.doubles.MixedDoublesDraw;
import wimbledon.entity.draw.doubles.WomensDoublesDraw;
import wimbledon.entity.draw.singles.MensSingleDraw;
import wimbledon.entity.draw.singles.QWomensSingleDraw;
import wimbledon.entity.draw.singles.WomensSingleDraw;
import wimbledon.entity.match.*;
import wimbledon.entity.player.Opponent;
import wimbledon.entity.player.Player;
import wimbledon.entity.team.MensDoublesTeam;
import wimbledon.entity.team.MixedDoublesTeam;
import wimbledon.entity.team.WomensDoublesTeam;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @author vrg
 */
@Singleton
@Startup
public class Initializer {

    @EJB
    private Initializer proxy;

    @PersistenceContext
    private EntityManager em;

    @Inject
    private JPAQueryFactory queryFactory;

    @PostConstruct
    private void init() {
        proxy.initAsync();
    }

    @Asynchronous
    public void initAsync() {
        proxy.createTestData();
//        JPAQueryFactory queryFactory = new JPAQueryFactory(new Provider<EntityManager>() {
//            @Override
//            public EntityManager get() {
//                return em;
//            }
//        });
        QWomensSingleDraw wsd = new QWomensSingleDraw("wsd");
        List<WomensSingleDraw> wsdList = queryFactory.from(wsd).limit(1).list(wsd);
        WomensSingleDraw d = wsdList.get(0);
        System.out.println("wsd: " + d);

        QSinglesMatch m = new QSinglesMatch("m");
        List<SinglesMatch> matches
                = queryFactory
                .from(m)
                .where(m.round.number.eq(1),
                        m.round.draw.eq(d))
                .list(m);
        System.out.println("matches in round 1: " + matches);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void createTestData() {
        List<Player> players = new ArrayList<>();
        Scanner scan = new Scanner(getClass().getResourceAsStream("/names.csv")).useDelimiter("\n");
        scan.next();
        while (scan.hasNext()) {
            String line = scan.next();
            String[] parts = line.split(",");
            Player p = new Player(parts[3] + " " + parts[4], Gender.valueOf(parts[6].toUpperCase()));

            players.add(p);

        }

        Umpire biroJanos = new Umpire("Biro Janos");
        Umpire biroFerenc = new Umpire("Biro Ferenc");

        Court pirosPalya = new Court("piros");
        Court kekPalya = new Court("kek");
        Court sargaPalya = new Court("sarga");
        Court zoldPalya = new Court("zold");

        MensDoublesDraw mdDraw = new MensDoublesDraw();
        mdDraw.addCourt(kekPalya).addCourt(pirosPalya).addUpire(biroFerenc);

        WomensDoublesDraw wdDraw = new WomensDoublesDraw();
        wdDraw.addCourt(kekPalya).addCourt(pirosPalya).addUpire(biroFerenc);

        MixedDoublesDraw mxdDraw = new MixedDoublesDraw();
        mxdDraw.addCourt(kekPalya).addCourt(pirosPalya).addUpire(biroFerenc);

        MensSingleDraw msDraw = new MensSingleDraw();
        msDraw.addCourt(kekPalya).addCourt(zoldPalya).addUpire(biroJanos);

        WomensSingleDraw wsDraw = new WomensSingleDraw();
        wsDraw.addCourt(kekPalya).addCourt(sargaPalya).addUpire(biroJanos);

        wsDraw.setStartDate(LocalDate.now().plusWeeks(5));
        wsDraw.setEndDate(LocalDate.now().plusWeeks(5).plusDays(10));


        try {
            List<Player> malePlayers = players.stream()
                    .filter(p -> p.getGender().equals(Gender.MALE))
                    .limit(16)
                    .collect(Collectors.toList());

            List<Player> femalePlayers = players.stream()
                    .filter(p -> p.getGender().equals(Gender.FEMALE))
                    .limit(16)
                    .collect(Collectors.toList());


            generateMatches(msDraw, malePlayers, SinglesMatch.class);

            generateMatches(wsDraw, femalePlayers, SinglesMatch.class);


            generateMatches(mdDraw, StreamUtils.zip(
                    malePlayers.stream().limit(8),
                    malePlayers.stream().skip(8).limit(8),
                    (a, b) -> new MensDoublesTeam(a, b)).collect(Collectors.toList()),
                    DoublesMatch.class);

            generateMatches(wdDraw, StreamUtils.zip(
                    femalePlayers.stream().limit(8),
                    femalePlayers.stream().skip(8).limit(8),
                    (a, b) -> new WomensDoublesTeam(a, b)).collect(Collectors.toList()),
                    DoublesMatch.class);

            generateMatches(mxdDraw, StreamUtils.zip(
                    femalePlayers.stream().limit(8),
                    malePlayers.stream().skip(8).limit(8),
                    (a, b) -> new MixedDoublesTeam(a, b)).collect(Collectors.toList()),
                    DoublesMatch.class);

        } catch (Exception e) {
            throw new RuntimeException("Can't create initial data", e);
        }


        em.persist(mdDraw);
        em.persist(wdDraw);
        em.persist(mxdDraw);
        em.persist(msDraw);
        em.persist(wsDraw);


    }


    private <O extends Opponent, M extends Match<O>> void generateMatches(Draw<O, M> draw, List<O> players, Class<? extends M> matchType) throws IllegalAccessException, InstantiationException {
        int roundSize = players.size();
        int round = 1;
        int playerIndex = 0;
        Round<O, M> prev = null;
        while (roundSize >= 2) {
            Round<O, M> r = new Round<O, M>();
            r.setDraw(draw);
            r.setNumber(round++);
            r.setMatches(new ArrayList<>());
            for (int m = 0; m < roundSize / 2; m++) {
                M match = matchType.newInstance();
                match.setRound(r);
                r.getMatches().add(match);
                match.setStatus(MatchStatus.NOT_PLAYED_YET);
                if (r.getNumber() == 1) {

                    match.setOpponent1(players.get(playerIndex++));
                    match.setOpponent2(players.get(playerIndex++));

                    draw.register(match.getOpponent1());
                    draw.register(match.getOpponent2());
                } else {

                    prev.getMatches().get(m * 2).setAdvancementSide(OpponentIndex.FIRST);
                    prev.getMatches().get(m * 2).setNextRoundMatch(match);
                    prev.getMatches().get(m * 2 + 1).setAdvancementSide(OpponentIndex.SECOND);
                    prev.getMatches().get(m * 2 + 1).setNextRoundMatch(match);
                }
            }
            draw.replaceRound(r);
            prev = r;
            roundSize = roundSize / 2;
        }
    }

}
