/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wimbledon.rest;

import java.math.BigDecimal;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import wimbledon.dao.OptimisticLockException;

/**
 *
 * @author vrg
 */
@Provider
public class OptimisticLockExceptionMapper implements ExceptionMapper<OptimisticLockException>{

    @Override
    public Response toResponse(OptimisticLockException exception) {
        JsonObject errorStructure = Json.createObjectBuilder()
                .add("message", "Conflict during merging entity")
                .add("entityName", exception.getEntityName())
                .add("id", exception.getId())
                .add("version", exception.getVersion())
                .add("dbVersion", exception.getDbVersion()).build();
        return Response.status(Response.Status.CONFLICT)
                .entity(errorStructure).build();
    }
    
}
