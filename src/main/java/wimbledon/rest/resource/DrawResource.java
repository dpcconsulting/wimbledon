package wimbledon.rest.resource;

import com.mysema.query.jpa.impl.JPAQueryFactory;
import wimbledon.entity.QRound;
import wimbledon.entity.Umpire;
import wimbledon.entity.draw.Draw;
import wimbledon.entity.draw.QDraw;
import wimbledon.rest.json.mapping.CourtMapper;
import wimbledon.rest.json.mapping.DrawMapper;
import wimbledon.rest.json.mapper.JSONUtil;
import wimbledon.rest.json.mapping.RoundMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * @author vrg
 */
@Path("/draws")
@Stateless
public class DrawResource {

    @Inject
    private EntityManager em;

    @Inject
    private JPAQueryFactory queryFactory;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray findAllJSON(@Context UriInfo uriInfo) {
        System.out.println("uriInfo: " + uriInfo);
        QDraw d = new QDraw("d");
        return queryFactory
                .from(d)
                .orderBy(d.id.asc())
                .list(d).stream().map(new DrawMapper()
                        .withAllBasic()
                        .without("version")
                )
                .collect(JSONUtil.toJsonArray());
    }

    @GET
    @Path("{id: \\d+}/rounds")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray findRounds(@PathParam("id") Long drawId) {
        Draw draw = em.find(Draw.class, drawId);

        QRound round = new QRound("r");
        return queryFactory
                .from(round)
                .orderBy(round.id.asc())
                .where(round.draw.eq(draw))
                .list(round).stream().map(new RoundMapper()
                        .withAllBasic()
                        .withMatches()
                )
                .collect(JSONUtil.toJsonArray());
    }

    @GET
    @Path("{id: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getDraw(@PathParam("id") Long id) {
        Draw draw = em.find(Draw.class, id);

        DrawMapper drawMapper = new DrawMapper()
                .withAllBasic()
                .withAvailableCourts()
                .mapAvailableCourts("availableCourtsNoVersion", new CourtMapper().withAllBasic().without("version"))
                .withAvailableUmpires()
                .mapAvailableUmpires("umpireNames", (JsonArrayBuilder b, Umpire u) -> b.add(u.getName()))
                .without("version");
        return drawMapper.apply(draw);
    }
}
