package wimbledon.rest.resource;

import wimbledon.dao.DAOBase;
import wimbledon.dao.EntityNotFoundException;
import wimbledon.entity.EntityBase;
import wimbledon.rest.json.mapper.EntityBaseMapper;

import javax.inject.Provider;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author vrg
 */
public class SubResource<E extends EntityBase, DAO extends DAOBase<E>> {

    private long id;

    private DAO dao;

    private Provider<EntityBaseMapper<E>> mapperProvider;

    public SubResource(DAO dao, Provider<EntityBaseMapper<E>> mapperProvider) {
        this.dao = dao;
        this.mapperProvider = mapperProvider;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EntityBaseMapper<E> createMapper() {
        return mapperProvider.get().withAllBasic().withAllCollectionURIs();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject get() {
        E entity = dao.findById(id);
        if (entity == null) {
            throw new EntityNotFoundException(dao.getEntityName(), id);
        }
        return createMapper().apply(entity);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response remove() {
        E removed = dao.remove(id);
        if (removed != null) {
            return Response.ok(dao.getEntityName() + " deleted with id " + id).build();
        } else {
            throw new EntityNotFoundException(dao.getEntityName(), id);
        }
    }

    protected void mergeAttributes(E detached, E managed) {
        managed.setVersion(detached.getVersion());
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public E merge(E stateToMerge) {
        E dbState = dao.findById(id);
        if (dbState == null) {
            throw new EntityNotFoundException(dao.getEntityName(), id);
        }
        mergeAttributes(stateToMerge, dbState);
        return dao.merge(dbState);
    }
}
