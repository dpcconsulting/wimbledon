package wimbledon.rest.resource;

import com.mysema.query.jpa.impl.JPAQueryFactory;
import wimbledon.entity.player.Player;
import wimbledon.entity.player.QPlayer;
import wimbledon.rest.json.mapper.JSONUtil;
import wimbledon.rest.json.mapping.PlayerMapper;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.json.JsonArray;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriBuilder;
import wimbledon.dao.PlayerDAO;

/**
 *
 * @author vrg
 */
@Path("/players")
@Singleton
public class PlayerResource extends Resource<Player, PlayerDAO> {

    public PlayerResource() {
        super(null, null, null);
    }
    
    @Inject
    public PlayerResource(JPAQueryFactory queryFactory, PlayerDAO dao, Provider<PlayerSubResource> subResourceProvider) {
        super(queryFactory, dao, (Provider) subResourceProvider);
    }

    @Override
    public JsonArray findAll(@Context UriBuilder uriBuilder) {
        QPlayer c = new QPlayer("p");
        return queryFactory
                .from(c)
                .orderBy(c.id.asc())
                .list(c).stream().map(new PlayerMapper().withAllBasic())
                .collect(JSONUtil.toJsonArray());
    }

}
