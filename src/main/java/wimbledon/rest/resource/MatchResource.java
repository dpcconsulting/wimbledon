package wimbledon.rest.resource;

import com.mysema.query.jpa.impl.JPAQueryFactory;
import wimbledon.dao.criteria.SetDTO;
import wimbledon.entity.match.Match;
import wimbledon.entity.match.Set;
import wimbledon.rest.json.mapping.MatchMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author vrg
 */
@Path("/matches")
@Stateless
public class MatchResource {

    @Inject
    private JPAQueryFactory queryFactory;

    @Inject
    private EntityManager em;


    @GET
    @Path("{id: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject findById(@PathParam("id") Long id) {
        final Match entity = em.find(Match.class, id);
        return new MatchMapper()
                .withAllBasic()
                .withSets()
                .withUmpire()
                .withCourt()
                .apply(entity);

    }

    @POST
    @Path("{id: \\d+}/set")
    @Produces(MediaType.APPLICATION_JSON)
    public void postSetResult(@PathParam("id") Long id, SetDTO set) {
        final Match match = em.find(Match.class, id);
        List<Set> sets = match.getSets();
        match.addSet(new Set(set.getScore1(), set.getScore2()));
        em.persist(match);

    }


}
