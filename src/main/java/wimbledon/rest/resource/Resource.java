package wimbledon.rest.resource;

import com.mysema.query.jpa.impl.JPAQueryFactory;
import java.net.URI;
import javax.inject.Provider;
import javax.json.JsonArray;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import wimbledon.dao.DAOBase;
import wimbledon.entity.EntityBase;
import wimbledon.rest.json.mapper.EntityUriMapper;

/**
 *
 * @author vrg
 */
public abstract class Resource<E extends EntityBase, DAO extends DAOBase<E>> {

    protected final JPAQueryFactory queryFactory;
    protected final DAO dao;
    protected final Provider<SubResource<E, DAO>> subResourceProvider;

    public Resource(JPAQueryFactory queryFactory, DAO dao, Provider<SubResource<E, DAO>> subResourceProvider) {
        this.queryFactory = queryFactory;
        this.dao = dao;
        this.subResourceProvider = subResourceProvider;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public abstract JsonArray findAll(@Context UriBuilder uriBuilder);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(E entity, @Context UriInfo uriInfo) {
        dao.persist(entity);
        URI uriOfNewEntity = new EntityUriMapper<>(uriInfo, getClass())
                .getURIOf(entity);
        return Response.created(uriOfNewEntity)
                .entity(dao.getEntityName() + " created").build();
    }

    @Path("{id: \\d+}")
    public SubResource<E, DAO> getSubResource(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        SubResource<E, DAO> subResource = subResourceProvider.get();
        subResource.setId(id);
        return subResource;
    }
}
