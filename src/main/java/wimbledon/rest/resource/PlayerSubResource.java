package wimbledon.rest.resource;

import wimbledon.entity.player.Player;

import javax.inject.Inject;
import javax.inject.Provider;
import wimbledon.dao.PlayerDAO;
import wimbledon.rest.json.mapping.PlayerMapper;

/**
 *
 * @author vrg
 */
public class PlayerSubResource extends SubResource<Player, PlayerDAO>{

    @Inject
    public PlayerSubResource(PlayerDAO dao, Provider<PlayerMapper> mapperProvider) {
        super(dao, (Provider)mapperProvider);
    }

    @Override
    protected void mergeAttributes(Player detached, Player managed) {
        super.mergeAttributes(detached, managed);
        managed.setName(detached.getName());
    }
    
    

}
