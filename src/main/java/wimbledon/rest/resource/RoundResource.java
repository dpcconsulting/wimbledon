package wimbledon.rest.resource;

import com.mysema.query.jpa.impl.JPAQueryFactory;
import wimbledon.entity.Round;
import wimbledon.rest.json.mapping.RoundMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author vrg
 */
@Path("/rounds")
@Stateless
public class RoundResource {

    @Inject
    private EntityManager em;

    @Inject
    private JPAQueryFactory queryFactory;



    @GET
    @Path("{id: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject getRound(@PathParam("id") Long id) {
        Round round = em.find(Round.class, id);

        RoundMapper mapper = new RoundMapper()
                .withAllBasic()
                .withMatches();

        return mapper.apply(round);
    }
}
