package wimbledon.rest.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author vrg
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(wimbledon.rest.EntityNotFoundExceptionMapper.class);
        resources.add(wimbledon.rest.OptimisticLockExceptionMapper.class);
        resources.add(wimbledon.rest.PersistenceExceptionMapper.class);
        resources.add(wimbledon.rest.RollbackExceptionMapper.class);
        resources.add(wimbledon.rest.resource.CourtResource.class);
        resources.add(wimbledon.rest.resource.DrawResource.class);
        resources.add(wimbledon.rest.resource.MatchResource.class);
        resources.add(wimbledon.rest.resource.PlayerResource.class);
        resources.add(wimbledon.rest.resource.RoundResource.class);
        resources.add(wimbledon.rest.resource.SubResource.class);

    }
    
}
