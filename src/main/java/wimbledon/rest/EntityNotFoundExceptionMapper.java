/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wimbledon.rest;

import wimbledon.dao.EntityNotFoundException;
import java.math.BigDecimal;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author vrg
 */
@Provider
public class EntityNotFoundExceptionMapper implements ExceptionMapper<EntityNotFoundException>{

    @Override
    public Response toResponse(EntityNotFoundException exception) {
        JsonObject errorStructure = Json.createObjectBuilder()
                .add("message", "Could not found entity")
                .add("entityName", exception.getEntityName())
                .add("id", exception.getId()).build();
        return Response.status(Response.Status.NOT_FOUND)
                .entity(errorStructure).build();
    }
    
}
