package wimbledon.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.PersistenceException;
import javax.transaction.RollbackException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author vrg
 */
@Provider
public class RollbackExceptionMapper implements ExceptionMapper<RollbackException>{

    public <EX extends Throwable> EX findCauseWithType(Class<EX>type, Throwable ex) {
        if (type.isInstance(ex)) {
            return (EX) ex;
        } else {
            Throwable cause = ex.getCause();
            if (cause == null) {
                return null;
            }
            return findCauseWithType(type, cause);
        }
    }
    
    @Override
    public Response toResponse(RollbackException exception) {
        Throwable relevantEx = exception;
        SQLIntegrityConstraintViolationException cvex = findCauseWithType(SQLIntegrityConstraintViolationException.class, exception);
        if (cvex != null) {
            relevantEx = cvex;
        }
        JsonObject errorStructure = Json.createObjectBuilder()
                .add("message", relevantEx.getMessage())
                .build();
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorStructure).build();
    }
    
}
