package wimbledon.rest.json.mapping;

import wimbledon.entity.match.Match;
import wimbledon.entity.match.Set;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 * @author vrg
 */
public class MatchMapper extends EntityBaseMapper<Match> {

    @Override
    public MatchMapper withAllBasic() {
        super.withAllBasic();
        addBasic("name", (builder, entity) -> builder.add("name", entity.getScore()));
        add("score", (builder, entity) -> builder.add("score", entity.getScore()));
        add("status", (builder, entity) -> builder.add("status", entity.getStatus().toString()));
        add("info", ((builder, entity) -> builder.add("info", entity.getPartiesInfo())));
        return this;
    }

    public MatchMapper withSets() {
        addCollection("sets",
                (Match match) -> match.getSets(),
                new EntityBaseMapper<Set>() {
                    @Override
                    public EntityBaseMapper withAllBasic() {
                        super.withAllBasic();
                        addBasic("score1", (builder, entity) -> builder.add("score1", entity.getScore1()));
                        addBasic("score2", (builder, entity) -> builder.add("score2", entity.getScore2()));
                        return this;
                    }
                }.withAllBasic());
        return this;
    }


    public MatchMapper withUmpire() {
        addBasic("umpire", (builder, entity) -> builder.add("umpire", new UmpireMapper().apply(entity.getUmpire())));
        return this;
    }

    public MatchMapper withCourt() {
        addBasic("court", (builder, entity) -> builder.add("court", new CourtMapper().withAllBasic().apply(entity.getCourt())));
        return this;
    }


}
