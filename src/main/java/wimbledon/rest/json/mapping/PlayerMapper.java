package wimbledon.rest.json.mapping;

import wimbledon.entity.player.Player;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 * @author vrg
 */
public class PlayerMapper extends EntityBaseMapper<Player> {
    @Override
    public EntityBaseMapper<Player> withAllBasic() {
        super.withAllBasic();
        addBasic("name", (builder, entity) -> builder.add("name", entity.getName()));
        addBasic("gender", (builder, entity) -> builder.add("gender", entity.getGender()));
        return this;
    }
}
