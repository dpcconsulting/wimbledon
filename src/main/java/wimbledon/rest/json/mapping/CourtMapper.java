package wimbledon.rest.json.mapping;

import wimbledon.entity.Court;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 * @author vrg
 */
public class CourtMapper extends EntityBaseMapper<Court> {

    @Override
    public CourtMapper withAllBasic() {
        super.withAllBasic();
        addBasic("name", (builder, entity) -> {
            if (entity != null) {
                builder.add("name", entity.getName());
            }
        });
        return this;
    }

}
