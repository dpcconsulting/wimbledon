package wimbledon.rest.json.mapping;

import wimbledon.entity.Umpire;
import wimbledon.rest.JsonObjectBuilder;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 *
 * @author vrg
 */
public class UmpireMapper extends EntityBaseMapper<Umpire> {

    @Override
    public UmpireMapper withAllBasic() {
        super.withAllBasic();
        addBasic("name", (JsonObjectBuilder builder, Umpire entity)
                -> builder.add("name", entity.getName()));
        return this;
    }

}
