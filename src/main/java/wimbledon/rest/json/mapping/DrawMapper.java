package wimbledon.rest.json.mapping;

import java.util.function.BiConsumer;
import javax.json.JsonArrayBuilder;
import wimbledon.entity.Court;
import wimbledon.entity.Umpire;
import wimbledon.entity.draw.Draw;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 *
 * @author vrg
 */
public class DrawMapper extends EntityBaseMapper<Draw> {

    @Override
    public DrawMapper without(String name) {
        super.without(name);
        return this;
    }
    
    public DrawMapper withAvailableCourts() {
        addCollection("availableCourts", 
                (Draw draw) -> draw.getAvailableCourts(), 
                new CourtMapper().withAllBasic());
        return this;
    }
    
    public DrawMapper mapAvailableCourts(String targetName, BiConsumer<JsonArrayBuilder,Court>adder) {
        addCollection(targetName, 
                (Draw draw) -> draw.getAvailableCourts(), 
                adder);
        return this;
    }
    
    public DrawMapper withAvailableUmpires() {
        addCollection("availableUmpires", 
                (Draw draw) -> draw.getAvailableUmpires(), 
                new UmpireMapper().withAllBasic());
        return this;
    }
    
    public DrawMapper mapAvailableUmpires(String targetName, BiConsumer<JsonArrayBuilder,Umpire>adder) {
        addCollection(targetName, 
                (Draw draw) -> draw.getAvailableUmpires(), 
                adder);
        return this;
    }

    @Override
    public DrawMapper withAllBasic() {
        super.withAllBasic();
        addBasic("name", (builder,entity) -> builder.add("name", entity.getName()));
        addBasic("type", (builder,entity) -> builder.add("type", entity.getType()));
        addBasic("startDate", (builder,entity) -> builder.add("startDate", entity.getStartDate()));
        return this;
    }

}
