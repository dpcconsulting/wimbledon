package wimbledon.rest.json.mapping;

import wimbledon.entity.Round;
import wimbledon.rest.json.mapper.EntityBaseMapper;

/**
 * @author vrg
 */
public class RoundMapper extends EntityBaseMapper<Round> {


    @Override
    public RoundMapper withAllBasic() {
        super.withAllBasic();
        addBasic("number", (builder, entity) -> builder.add("name", entity.getNumber()));
        return this;
    }

    public RoundMapper withMatches() {
        addCollection("matches", (Round r) -> r.getMatches(), new MatchMapper().withAllBasic());
        return this;
    }
}
