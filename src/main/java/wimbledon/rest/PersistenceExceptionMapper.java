/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wimbledon.rest;

import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author vrg
 */
@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException>{

    @Override
    public Response toResponse(PersistenceException exception) {
        JsonObject errorStructure = Json.createObjectBuilder()
                .add("message", exception.getMessage())
                .build();
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorStructure).build();
    }
    
}
