package wimbledon.entity.match;

public enum OpponentIndex {

    FIRST, SECOND;
}
