package wimbledon.entity.match;

import wimbledon.entity.player.Player;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author vrg
 */
@Entity
@DiscriminatorValue("S")
@Table(name = "MATCH_SINGLES")
public class SinglesMatch extends Match<Player> {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Player opponent1;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Player opponent2;

    public SinglesMatch() {
    }

    public SinglesMatch(Player player1, Player player2) {
        Objects.requireNonNull(player1, "player1 must not be null");
        Objects.requireNonNull(player2, "player2 must not be null");
        this.opponent1 = player1;
        this.opponent2 = player2;
    }


    @Override
    public Player getOpponent1() {
        return opponent1;
    }


    @Override
    public Player getOpponent2() {
        return opponent2;
    }

    @Override
    public void setOpponent1(Player opponent1) {
        this.opponent1 = opponent1;
    }

    @Override
    public void setOpponent2(Player opponent2) {
        this.opponent2 = opponent2;
    }

    @Override
    public String getPartiesInfo() {
        if (opponent1 != null && opponent2 != null) {
            return opponent1.getName() + " vs " + opponent2.getName();
        } else {
            return super.getPartiesInfo();
        }
    }
}
