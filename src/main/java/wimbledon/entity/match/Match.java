package wimbledon.entity.match;

import wimbledon.entity.Court;
import wimbledon.entity.EntityBase;
import wimbledon.entity.Round;
import wimbledon.entity.Umpire;
import wimbledon.entity.player.Opponent;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vrg
 */
@Entity
@Table(name = "MATCH_COMMON")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue("0")
public abstract class Match<T extends Opponent> extends EntityBase {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Court court;

    private LocalDateTime startTime;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Umpire umpire;

    private MatchStatus status = MatchStatus.NOT_PLAYED_YET;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Round round;

    @ManyToOne
    private Match nextRoundMatch;

    private OpponentIndex advancementSide;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "MATCH_SET", joinColumns = @JoinColumn(name = "MATCH_ID"),
            inverseJoinColumns = @JoinColumn(name = "SET_ID"))
    private List<Set> sets = new ArrayList<>();

    public abstract T getOpponent1();

    public abstract void setOpponent1(T opponent);

    public abstract T getOpponent2();

    public abstract void setOpponent2(T opponent);

    public void setOpponent(OpponentIndex index, T opponent) {
        if (index == OpponentIndex.FIRST) {
            setOpponent1(opponent);
        } else {
            setOpponent2(opponent);
        }
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public Umpire getUmpire() {
        return umpire;
    }

    public void setUmpire(Umpire umpire) {
        this.umpire = umpire;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public List<Set> getSets() {
        return sets;
    }

    public void setSets(List<Set> sets) {
        this.sets = sets;
    }

    public Match addSet(Set set) {
        sets.add(set);
        if (status == MatchStatus.NOT_PLAYED_YET) {
            status = MatchStatus.NOW_PLAYING;
        }
        if (sets.size() >= 3) {
            int result = sets.stream().mapToInt(s -> s.getResult()).sum();

            if (result > 1) {
                //winner is the opponent 2
                setStatus(MatchStatus.SECOND_WON);

                if (getNextRoundMatch() != null) {
                    getNextRoundMatch()
                            .setOpponent(getAdvancementSide(), getOpponent2());
                }

            } else if (result < -1) {
                //winner is the opponent 1
                setStatus(MatchStatus.FIRST_WON);

                if (getNextRoundMatch() != null) {
                    getNextRoundMatch()
                            .setOpponent(getAdvancementSide(), getOpponent1());
                }

            }
        }
        return this;
    }

    public OpponentIndex getAdvancementSide() {
        return advancementSide;
    }

    public void setAdvancementSide(OpponentIndex advancementSide) {
        this.advancementSide = advancementSide;
    }

    public Match getNextRoundMatch() {
        return nextRoundMatch;
    }

    public void setNextRoundMatch(Match nextRoundMatch) {
        this.nextRoundMatch = nextRoundMatch;
    }

    public String getScore() {
        return sets.stream().map(s -> s.toString()).collect(Collectors.joining(" "));
    }

    public String getPartiesInfo() {
        if (getOpponent1() != null && getOpponent2() != null) {
            return "Winners of " + getOpponent1() + ":" + getOpponent2();
        } else {
            return "Unknown";
        }
    }

    @Override
    public String toString() {
        return "Match{" + "parties=" + getPartiesInfo()
                + ", status=" + status
                + ", score: " + getScore()
                + ", court=" + court
                + ", startTime=" + startTime
                + ", umpire=" + umpire + '}';
    }

}
