package wimbledon.entity.match;

import wimbledon.entity.team.Team;

import javax.persistence.*;

/**
 * @author vrg
 */
@Entity
@DiscriminatorValue("D")
@Table(name = "MATCH_DOUBLES")
public class DoublesMatch extends Match<Team> {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Team opponent1;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Team opponent2;

    public DoublesMatch() {
    }

    public DoublesMatch(Team opponent1, Team opponent2) {
        this.opponent1 = opponent1;
        this.opponent2 = opponent2;
    }

    public Team getOpponent1() {
        return opponent1;
    }

    public Team getOpponent2() {
        return opponent2;
    }

    public void setOpponent1(Team opponent1) {
        this.opponent1 = opponent1;
    }

    public void setOpponent2(Team opponent2) {
        this.opponent2 = opponent2;
    }

    @Override
    public String getPartiesInfo() {
        return getInfo(opponent1) + " vs " + getInfo(opponent2);
    }

    private String getInfo(Team opponent) {
        return opponent == null ? "?" : opponent.getPlayer1().getName() + "+" + opponent.getPlayer2().getName();
    }


}
