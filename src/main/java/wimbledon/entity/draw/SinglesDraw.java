package wimbledon.entity.draw;

import wimbledon.entity.match.SinglesMatch;
import wimbledon.entity.player.Player;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vrg
 */
@Entity
@Table(name = "DRAW_SINGLES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class SinglesDraw extends Draw<Player, SinglesMatch> {

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "DRAW_SINGLES_PLAYER",
            joinColumns = @JoinColumn(name = "DRAW_ID"),
            inverseJoinColumns = @JoinColumn(name = "PLAYER_ID"))
    protected List<Player> registeredPlayers = new ArrayList<>();

    @Override
    public SinglesDraw register(Player player) {
        registeredPlayers.add(player);
        return this;
    }
}
