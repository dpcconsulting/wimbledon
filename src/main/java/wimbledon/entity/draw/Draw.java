package wimbledon.entity.draw;

import wimbledon.entity.*;
import wimbledon.entity.match.Match;
import wimbledon.entity.player.Opponent;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vrg
 */
@Entity(name = "DRAW")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Draw<T extends Opponent, M extends Match<T>> extends EntityBase {

    private String name;

    private LocalDate startDate;

    private LocalDate endDate;

    @OneToMany(mappedBy = "draw", cascade = CascadeType.PERSIST)
    @MapKey(name = "number")
    private Map<Integer, Round<T, M>> rounds = new HashMap<>();

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "DRAW_COURT",
            joinColumns = @JoinColumn(name = "DRAW_ID"),
            inverseJoinColumns = @JoinColumn(name = "COURT_ID"))
    private List<Court> availableCourts = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            uniqueConstraints = {},
            name = "DRAW_UMPIRE",
            joinColumns = @JoinColumn(name = "DRAW_ID"),
            inverseJoinColumns = @JoinColumn(name = "UMPIRE_ID"))
    private List<Umpire> availableUmpires = new ArrayList<>();

    public abstract DrawType getType();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Round<T, M> getRound(int number) {
        return rounds.get(number);
    }

    public Draw<T, M> replaceRound(Round<T, M> round) {
        this.rounds.put(round.getNumber(), round);
        round.setDraw(this);
        return this;
    }

    public List<Court> getAvailableCourts() {
        return availableCourts;
    }

    public void setAvailableCourts(List<Court> availableCourts) {
        this.availableCourts = availableCourts;
    }

    public Draw addCourt(Court court) {
        availableCourts.add(court);
        return this;
    }

    public Draw addUpire(Umpire umpire) {
        availableUmpires.add(umpire);
        return this;
    }

    public abstract Draw<T, M> register(T opponent);

    public List<Umpire> getAvailableUmpires() {
        return availableUmpires;
    }

    public void setAvailableUmpires(List<Umpire> availableUmpires) {
        this.availableUmpires = availableUmpires;
    }

    @Override
    public String toString() {
        return "Draw{" + "name=" + name + ", type=" + getType() + ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }

}
