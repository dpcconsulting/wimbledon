package wimbledon.entity.draw;

import wimbledon.entity.match.DoublesMatch;
import wimbledon.entity.team.Team;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vrg
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "DRAW_DOUBLES")
public abstract class DoublesDraw extends Draw<Team, DoublesMatch> {

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "DRAW_DOUBLES_TEAM",
            joinColumns = @JoinColumn(name = "DRAW_ID"),
            inverseJoinColumns = @JoinColumn(name = "TEAM_ID"))
    protected List<Team> registeredTeams = new ArrayList<>();

    public DoublesDraw register(Team team) {
        registeredTeams.add(team);
        return this;
    }

}
