package wimbledon.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import wimbledon.entity.EntityBase;

/**
 *
 * @author vrg
 * @param <E> Entity type
 */
public abstract class DAOBase<E extends EntityBase> {

    protected final EntityManager em;

    private final Class<E> entityType;

    @Inject
    private Event<EntityCreatedEvent<E>> entityCreated;

    public DAOBase(Class<E> entityType, EntityManager em) {
        this.em = em;
        this.entityType = entityType;
    }

    public E findById(Long id) {
        return em.find(entityType, id);
    }

    public String getEntityName() {
        return entityType.getSimpleName();
    }

    @Transactional
    public E persist(E entity) {
        em.persist(entity);
        entityCreated.fire(new EntityCreatedEvent<>(entity));
        return entity;
    }

    @Transactional
    public E merge(E entity) {
        try {
            return em.merge(entity);
        } catch (javax.persistence.OptimisticLockException ex) {
            E dbVersion = em.find(entityType, entity.getId());
            throw new OptimisticLockException(getEntityName(), entity.getId(), entity.getVersion(), dbVersion.getVersion(), ex);
        }
    }

    @Transactional
    public E remove(E entity) {
        return remove(entity.getId());
    }

    @Transactional
    public E remove(Long id) {
        try {
            E managed = em.getReference(entityType, id);
            em.remove(managed);
            return managed;
        } catch (javax.persistence.EntityNotFoundException ex) {
            throw new EntityNotFoundException(getEntityName(), id);
        }
    }

    public E clone(E entity) {
        try {
            E cloned = entityType.newInstance();
            cloned.setId(entity.getId());
            cloned.setVersion(entity.getVersion());
            return entity;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("Unexpected exception during cloning entity: " + entity, ex);
        }
    }

    public <T> T getFirstResult(CriteriaQuery<T> cq) {
        List<T> resultList = em.createQuery(cq).setMaxResults(1).getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList.get(0);
        }
    }

}
