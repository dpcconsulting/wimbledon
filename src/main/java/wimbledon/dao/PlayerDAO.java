package wimbledon.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import wimbledon.entity.player.Player;

/**
 *
 * @author vrg
 */
public class PlayerDAO extends DAOBase<Player>{

    @Inject
    public PlayerDAO(EntityManager em) {
        super(Player.class, em);
    }
}
