package wimbledon.dao;

import wimbledon.entity.EntityBase;

/**
 *
 * @author vrg
 */
public class EntityCreatedEvent<E extends EntityBase> {
    private final E entity;

    public EntityCreatedEvent(E entity) {
        this.entity = entity;
    }

    public E getEntity() {
        return entity;
    }
    
}
