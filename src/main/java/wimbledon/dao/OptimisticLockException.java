package wimbledon.dao;

import javax.ejb.ApplicationException;

/**
 *
 * @author vrg
 */
@ApplicationException
public class OptimisticLockException extends RuntimeException {

    private final String entityName;
    private final Long id;
    private final Long version;
    private final Long dbVersion;

    public OptimisticLockException(String entityName, Long id, Long version, Long dbVersion, Throwable cause) {
        super("Conflict during merging " + entityName + "with id=" + id + ", version=" + version + ", dbVersion=" + dbVersion, cause);
        this.entityName = entityName;
        this.id = id;
        this.version = version;
        this.dbVersion = dbVersion;
    }

    public String getEntityName() {
        return entityName;
    }

    public Long getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public Long getDbVersion() {
        return dbVersion;
    }

    
    
}
