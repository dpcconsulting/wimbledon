package wimbledon.dao;

import javax.ejb.ApplicationException;

/**
 *
 * @author vrg
 */
@ApplicationException
public class EntityNotFoundException extends RuntimeException {

    private final String entityName;
    private final long id;
    
    public EntityNotFoundException(String entityName, long id) {
        super(entityName + " not found with id " + id);
        this.entityName = entityName;
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public long getId() {
        return id;
    }
}
