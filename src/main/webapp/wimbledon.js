angular.module('wimbledon', ['ui.router'])
    .config(function ($httpProvider) {
        $httpProvider.defaults.withCredentials = true;

        $httpProvider.interceptors.push(function () {
            return {
                'responseError': function (response) {
                    alert("ERROR: " + response.status + " " + response.statusText);
                },
                'request': function (config) {
                    //TODO handle context path
                    if (!config.url.endsWith("html")) {
                        //config.url = 'Wimbledon/' + config.url;
                    }
                    return config;
                }

            };
        });
    }).config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/courts");

        $stateProvider
            .state('courts', {
                url: "/courts",
                templateUrl: "courts.html",
                controller: 'courts'
            })
            .state('draws', {
                url: "/draws",
                templateUrl: "draws.html",
                controller: function ($scope, $http) {
                    $http.get("rest/draws").then(function (data) {
                        $scope.draws = data.data;
                    })
                }
            })
            .state('draw', {
                url: "/draw/:id",
                templateUrl: "draw.html",
                controller: function ($scope, $http, $stateParams) {
                    $http.get("rest/draws/" + $stateParams.id).then(function (data) {
                        $scope.draw = data.data;
                        $http.get("rest/draws/" + $stateParams.id + '/rounds').then(function (data) {
                            $scope.draw.rounds = data.data;
                        });
                    })
                }
            })
            .state('round', {
                url: "/round/:id",
                templateUrl: "round.html",
                controller: function ($scope, $http, $stateParams) {
                    $http.get("rest/rounds/" + $stateParams.id).then(function (data) {
                        $scope.round = data.data;
                    })
                }
            })
            .state('match', {
                url: "/match/:id",
                templateUrl: "match.html",
                controller: function ($scope, $http, $stateParams) {
                    $http.get("rest/matches/" + $stateParams.id).then(function (data) {
                        $scope.match = data.data;
                    })
                    $scope.sendResults = function () {
                        var scores = {'score1': $scope.score1, 'score2': $scope.score2};
                        $http.post("rest/matches/" + $scope.match.id + "/set", scores).then(function (data) {
                            alert("result has been saved");
                            $http.get("rest/matches/" + $scope.match.id).then(function (data) {
                                $scope.match = data.data;
                                $scope.score1 = '';
                                $scope.score2 = '';
                            })
                        })
                    }
                }
            })
            .state('players', {
                url: "/players",
                templateUrl: "players.html",
                controller: function ($scope, $http) {
                    $http.get("rest/players").then(function (data) {
                        $scope.players = data.data;
                    })
                }
            })
            .state('player', {
                url: "/player/:id",
                templateUrl: "player.html",
                controller: function ($scope, $http, $stateParams) {
                    $http.get("rest/players/" + $stateParams.id).then(function (data) {
                        $scope.player = data.data;
                    })
                }
            })
            .state('player-edit', {
                url: "/player/edit/:id",
                templateUrl: "player-edit.html",
                controller: function ($scope, $http, $stateParams, $state) {
                    $http.get("rest/players/" + $stateParams.id).then(function (data) {
                        $scope.player = data.data;
                    })
                    $scope.save = function () {
                        $http.put("rest/players/" + $stateParams.id, $scope.player).then(function (data) {
                            $state.go('player', {id: $stateParams.id});
                        })
                    }
                }
            })

    })

    .controller("courts", function ($scope, $http) {
        $http.get("rest/courts").then(function (data) {
            $scope.courts = data.data;
        })
    });

